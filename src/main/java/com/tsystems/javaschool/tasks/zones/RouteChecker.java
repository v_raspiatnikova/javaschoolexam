package com.tsystems.javaschool.tasks.zones;

import java.util.*;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        // set number of vertices in a graph as the number of zones
        int nVertices = zoneState.size();
        // set the dimension of the adjacency matrix
        int[][] adjMat = new int[nVertices][nVertices];
        // create the list of visited zones
        List<Integer> visitedZones = new ArrayList<>();
        // create the list of zone's ids
        List<Integer> zoneIds = new ArrayList<>(zoneState.size());
        for (Zone zone: zoneState){
            zoneIds.add(zone.getId());
        }
        // sorting requestedZoneIds in ascending order
        Collections.sort(requestedZoneIds);

        // check requestedZoneIds
        for (int i: requestedZoneIds) {
            if (!zoneIds.contains(i)) return false;
        }
        // fill the adjacency matrix
        for (Zone zone: zoneState){
            for (int i: zone.getNeighbours()) {
                addEdge(zone.getId(), i, adjMat);
            }
        }
        // add the first zone from requestedZoneIds in visitedZones
        visitedZones.add(requestedZoneIds.get(0));

        // pass through requestedZoneIds
        for (int i = 0; i <requestedZoneIds.size()-1 ; i++) {
            // if two zones are connected add the second in visitedZones
            if (adjMat[requestedZoneIds.get(i)-1][requestedZoneIds.get(i+1)-1] == 1){
                visitedZones.add(requestedZoneIds.get(i+1));
            }
            //else pass through visitedZones
            else{
                for (int j = 0; j < visitedZones.size() ; j++) {
                    // if the second zone is connected to one of visitedZones return true
                    if (adjMat[requestedZoneIds.get(i+1)-1][visitedZones.get(j)-1] == 1) return true;
                }
            }
        }
        // if visitedZones equal to requestedZoneIds return true
        if (visitedZones.containsAll(requestedZoneIds)) return true;
        return false;
    }

    /**
     * add edge between start and end vertices in a graph
     * @param start start vertex
     * @param end  end vertex
     * @param adjMat adjacency matrix where we add an edge
     */
    public void addEdge(int start, int end, int[][] adjMat){
        adjMat[start-1][end-1] = 1;
        adjMat[end-1][start-1] = 1;
    }
}
